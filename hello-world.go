package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type server struct{}

/*
GetName : retorna el nombre que viene en el request
*/
func GetName(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	nombre := params["name"]

	log.Println(" *** name : ", nombre)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("{\"message\": \"Hello world " + nombre + "\"}"))
}

func main() {
	router := mux.NewRouter()
	log.Println(`start 
	on port 5000 `)
	router.HandleFunc("/hello-world/{name}", GetName).Methods("GET")
	//En caso que el programa falle, baja el servicio
	log.Fatal(http.ListenAndServe(":5000", router))
}

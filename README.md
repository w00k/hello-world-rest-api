# Simple Hello World Example in Golang

This is a simple example for microservices in Golang, for testing purposes. 

## Running it
You can run it. 
go run hello-world.go

Build excecute this command.

* For Linux 

```bash
<in_you_path>$ go build -o hello-world-rest-api hello-world.go 
```
* For Windows

```bash
<in_you_path>: go build -o hello-world-rest-api.exe hello-world
```

## Testing

For testing purposes, in the browser or Postman follow the URL

```bash
http://127.0.0.1:5000/hello-world/{put_your_name}
```

Like this example 

```bash
http://127.0.0.1:5000/hello-world/Leif
```

